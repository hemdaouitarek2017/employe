/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Index from "views/Index.js";
import Maps from "views/examples/Maps.js";
import Register from "views/examples/Register.js";
import Login from "views/examples/Login.js";
import Tables from "views/examples/Tables.js";
import Icons from "views/examples/Icons.js";
import Rentclient from "views/examples/Rentclient.js";
import EditRentClient from "views/examples/EditRentClient"
import RentClientAppr from 'views/examples/RentClientAppr'
import Appreil from "views/examples/Appreil"
import FicheFinale from "views/examples/FicheFinale"
var routes = [
  {
    path: "/ModifierAppreil",
    name: "Modifier Appreil",
    icon: "ni ni-planet text-blue",
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/listegarantie",
    name: "Liste des contrats",
    icon: "ni ni-bullet-list-67 text-red",
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/Clients",
    name: "Liste des clients",
    icon: "ni ni-bullet-list-67 text-red",
    component: Rentclient,
    layout: "/admin"
  },
  {
    path: "/tables",
    name: "Liste des appreils",
    icon: "ni ni-bullet-list-67 text-red",
    component: Tables,
    layout: "/admin"
  },
  {
    path: "/EditRentClient",
    name: "EditRent",
    icon: "ni ni-bullet-list-67 text-red",
    component: EditRentClient,
    layout: "/admin"
  },
  {
    path: "/login",
    name: "Login",
    icon: "ni ni-key-25 text-info",
    component: Login,
    layout: "/auth"
  },
  {
    path: "/register",
    name: "Register",
    icon: "ni ni-circle-08 text-pink",
    component: Register,
    layout: "/auth"
  },
  {
    path: "/Add",
    name: "Ajout un garantie",
    icon: "ni ni-circle-08 text-pink",
    component: RentClientAppr,
    layout: "/admin"
  },
  {
    path: "/appreil",
    name: "Appreil",
    icon: "ni ni-circle-08 text-pink",
    component: Appreil,
    layout: "/admin"
  },
  {
    path: "/FicheFinale",
    name: "Fiche Finale",
    icon: "ni ni-circle-08 text-pink",
    component: FicheFinale,
    layout: "/add"
  },
];
export default routes;
