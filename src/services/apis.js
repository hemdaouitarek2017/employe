let server='http://41.226.248.55:8080/'
let login='api/auth/signin'
let rentClient='apis/RenClient'
let RenVehicule='apis/RenVehicule'
let PeriodeCouverture='apis/PeriodeCouverture'
let assurance='apis/Assurance/'
let rentClientAppr='apis/RentClientAppr/'
let Appreil='apis/Appreil/'
let role='api/auth/getRole/'
let garantie="apis/GarantieDistrubuteur/"
let appreilRevendeur="apis/AppreilByRevendeur/"
let getgaranties="apis/GarantieByRevendeur/"
let gar="apis/Garantie/"
let distrubuteur="apis/Distrubuteur"
let rentClientRevendeur="apis/RentClientApprByReven/"
let filt="apis/searchfilt"
let garEmploye="apis/GarantieByEmploye/"
let appreilEmp="apis/AppreilByEmploye/"
let rentClientEmp="apis/RentClientApprByEmploye/"
class tools {
    getRentEmpl(id){
        return server+rentClientEmp+id
    }
    getAppreilEmp(id){
        return server+appreilEmp+id
    }
    getgarantieByEmploye(ud){
        return server+garEmploye+ud
    }
    filter(){
        return server+filt
    } 
   deleteRentClient(id){
        return server+rentClientAppr+id
    }
    getrentClient(id){
        return server+rentClientRevendeur+id
    }
    getDistrubuteur(id){
        return server+distrubuteur
    }
    garant(id){
        return server+gar+id
    }
    getgaranties(id){
        return server+getgaranties+id
    }
    appreilRevendeur(id){
        return server+appreilRevendeur+id
    }
    garantie(id){
        return server+garantie+id
    }
    getrole(username){
        return server+role+username
    }
    signin(){
        return server+login;
    }
    rentClient(){
        return server+rentClient
    }
    RenVehicule(){
        return server+RenVehicule
    }
    PeriodeCouverture(){
        return server+PeriodeCouverture
    }
    setAssur(id,idveh,idper){
        return server+assurance+id+"/"+idveh+"/"+idper
    }
    setrentClientAppr(ud){
        return server+rentClientAppr+ud
    }
    setAppreil(id){
        return server+Appreil+id
    }
}
const tool = new tools();
export default tool;