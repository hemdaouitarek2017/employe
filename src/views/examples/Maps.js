/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// react plugin used to create google maps
import {
  Badge,
  Card,
  CardHeader,
  Pagination,
  PaginationItem,
  PaginationLink,
  Button,
  Table,
  Container,
  Row,
} from "reactstrap";
// core components
import Header from "components/Headers/Header.js";
import tools from "../../services/apis.js"
import edit from "../../assets/img/edit.png"
import del from "../../assets/img/delete.png"
// mapTypeId={google.maps.MapTypeId.ROADMAP}


class Maps extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 0,
      ListGaranties: []
    };
  }
  handleClick(e, index) {

    e.preventDefault();

    this.setState({
      currentPage: index
    });

  }
  componentWillMount() {
    this.getgaranties()
    this.pageSize = 10;
  }
  async getgaranties() {
    await fetch(tools.getgarantieByEmploye(1), {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(results => {
        return results.json()
      }).then(data => {
        console.log(data)
        this.pagesCount = Math.ceil(data.length / this.pageSize);
        this.setState({ ListGaranties: data })
      })
  }
  async garantie(id) {
    await fetch(tools.garant(id), {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(results => {
        return results
      }).then(data => {
        alert("Le garantie a éte supprime")
        console.log(data)
        this.setState({
          ListGaranties: this.state.ListGaranties.filter(item => item.id !== id),
        });
      })
  }
  showRes(item) {
    return (
      <tr>
        <th scope="row">
          <a className="mb-0 text-sm">{item.id}</a>
        </th>
        <th scope="row">
          <a  className="mb-0 text-sm">{item.appreil.dateAchat}</a>
        </th>
        <th scope="row">
          <a  className="mb-0 text-sm">{item.rentClientAppr.nom + " " + item.rentClientAppr.prenom}</a>
        </th>
        <th>
          <a ><span className="mb-0 text-sm">{item.appreil.marque + " " + item.appreil.modéle}</span></a>
        </th>
        <th scope="row">
          <a  className="mb-0 text-sm">{item.appreil.prix}</a>
        </th>
        <th scope="row">
          <a  className="mb-0 text-sm">{item.appreil.prime}</a>
        </th>
       
      </tr>
    )
  }
  render() {
    const { currentPage } = this.state;
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Table */}
          <Row>
            <div className="col">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <h3 className="mb-0">Liste des affaires</h3>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Contract</th>
                      <th scope="col">Date d'achat</th>
                      <th scope="col">Client</th>
                      <th scope="col">Appreil</th>
                      <th scope="col">Prix</th>
                      <th scope="col">Prime</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.state.ListGaranties.slice(
                        currentPage * this.pageSize,
                        (currentPage + 1) * this.pageSize
                      ).map((t) => this.showRes(t))
                    }
                  </tbody>
                </Table>
                <div className="pagination-wrapper">
                  <Pagination style={{ padding: 10 }} className="pagination justify-content-end mb-0"
                    listClassName="justify-content-end mb-0">
                    <PaginationItem disabled={currentPage <= 0}>
                      <PaginationLink
                        onClick={e => this.handleClick(e, currentPage - 1)}
                        previous
                        href="#"
                      />
                    </PaginationItem>
                    {[...Array(this.pagesCount)].map((page, i) =>
                      <PaginationItem active={i === currentPage} key={i}>
                        <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                          {i + 1}
                        </PaginationLink>
                      </PaginationItem>
                    )}
                    <PaginationItem disabled={currentPage >= this.pagesCount - 1}>
                      <PaginationLink
                        onClick={e => this.handleClick(e, currentPage + 1)}
                        next
                        href="#"
                      />
                    </PaginationItem>
                  </Pagination>
                </div>
              </Card>
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

export default Maps;
