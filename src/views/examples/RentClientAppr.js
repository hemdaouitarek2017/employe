import React, { Component } from 'react';
import {
    Card,
    CardHeader,
    CardBody,
    Container,
    Col,
    FormGroup,
    Input,
    Row, Button
} from "reactstrap";
import { Radio, Form } from 'semantic-ui-react'
import Header from "components/Headers/Header.js";

import tools from "../../services/apis.js"
import { BrowserRouter, Route, Link } from 'react-router-dom'

class RentClientAppr extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            nom: '',
            Prenom: '',
            Raison: '',
            RC: '',
            Adresse: '',
            Rue: '',
            Code: '',
            NuméroTel: '',
            Modéle: '',
            NumIdentite: '',
            idVeh: null,
            naissance: new Date(),
            value: '',
            ville: '',
            Télephone: '',
            idrev: null
        }
    }
    handleChange1() {
        this.setState({ Sexe: "Mr" })
    }
    handleChange2() {
        this.setState({ Sexe: 'Me' })
    }
    async ajout() {
        if (this.state.nom == "" || this.state.Prenom == "" || this.state.NumIdentite == "") {
            alert("Voulez vous remplir tous les champs")
        }
        else if (this.state.NumIdentite.length != 8) {
            alert("CIN incorrect")
        }
        else {
            await fetch(tools.setrentClientAppr(1), {
                method: 'POST',
                body: JSON.stringify({
                    "nom": this.state.nom,
                    "prenom": this.state.Prenom,
                    "identite": this.state.NumIdentite,
                    "telephone": this.state.NuméroTel,
                    "sexe": this.state.value,
                    "ville": this.state.ville,
                    "adresse": this.state.Adresse
                }),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(results => {
                    return results.json()
                }).then(data => {
                    console.log(data)
                    this.props.history.replace(`/admin/appreil`, { idClient: data.id, idrev: this.state.idrev })
                })
        }

    }
    componentWillMount() {
        this.setState({ idrev: 1 })
    }
    handleChange = (e, { value }) => this.setState({ value })
    render() {
        return (
            <>
                <Header />
                {/* Page content */}
                <Container className="mt--7" fluid>
                    {/* Table */}
                    <Row>
                        <div className="col">
                            <Card className="shadow">
                                <div style={{ marginTop: '5%' }}>
                                    <Form>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup >
                                                    <Input
                                                        value={this.state.NumIdentite}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Numére CIN"
                                                        type="text"
                                                        onChange={(text) => this.setState({ NumIdentite: text.target.value })}
                                                    />
                                                </FormGroup>
                                                <FormGroup >
                                                    <Input
                                                        value={this.state.nom}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Nom"
                                                        type="text"
                                                        onChange={(text) => this.setState({ nom: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.Prenom}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Prénom"
                                                        onChange={(text) => this.setState({ Prenom: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.NuméroTel}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Numéro Téléphone"
                                                        onChange={(text) => this.setState({ NuméroTel: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.ville}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Ville"
                                                        onChange={(text) => this.setState({ ville: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.Adresse}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Adresse"
                                                        onChange={(text) => this.setState({ Adresse: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Form.Group style={{ marginLeft: 65, flexDirection: "row" }}>
                                                <Form.Radio
                                                    label='Homme'
                                                    value='Homme'
                                                    checked={this.state.value === 'Homme'}
                                                    onChange={this.handleChange}
                                                />
                                                <Form.Radio
                                                    label='Femme'
                                                    value='Femme'
                                                    checked={this.state.value === 'Femme'}
                                                    onChange={this.handleChange}
                                                />
                                            </Form.Group>
                                        </Row>
                                    </Form>
                                    <Button
                                        color=" "
                                        type="button"
                                        style={{ width: "20%", marginLeft: "64%", marginTop: 20, marginBottom: 20, backgroundColor: "#fe0c00" }}
                                        onClick={() => this.ajout()}
                                    >
                                        <span style={{ color: "#fff" }}>Suivant</span>
                                    </Button>
                                </div>
                            </Card>
                        </div>
                    </Row>
                </Container>
            </>
        );

    }
}

const style = {
    margin: 15,

};

export default RentClientAppr;
