import React, { Component } from 'react';
import {
    Card,
    CardHeader,
    CardBody,
    Container,
    Col,
    FormGroup,
    Input,
    Row, Button,
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem
} from "reactstrap";
import { Radio, Form } from 'semantic-ui-react'
import tools from "../../services/apis.js"
import ReactDatetime from "react-datetime";
import moment from "moment";
import Header from "components/Headers/Header.js";

class Appreil extends Component {
    constructor(props) {
        super(props);
        this.state = {
            marque: 'Choisie le marque de telephone',
            modéle: "",
            prixDachat: "",
            dateAchat: moment(),
            value: 'Choisie le type de telephone',
            imei: "",
            prime: 0.0,
            idclient: '',
            type: '',
            idrev: null,
            drop2: false
        }
    }
    gsm() {
        this.setState({ value: "GSM" })
    }
    smart() {
        this.setState({ value: "SMARTPHONE" })
    }
    tab() {
        this.setState({ value: "TABLETTE NUMERIQUE" })
    }
    async ajout() {

        if (this.state.marque == "Choisie le type de telephone" || this.state.modéle == "" || this.state.prixDachat == "") {
            alert("Voulez vous remplir tous les champs")
        }
        else if (this.state.imei.length != 15) {
            alert("IMEI incorrect")
        }
        else {
            await fetch(tools.setAppreil(1), {
                method: 'POST',
                body: JSON.stringify({
                    "dateAchat": moment(this.state.dateAchat).format("YYYY-MM-DD"),
                    "marque": this.state.marque,
                    "modéle": this.state.modéle,
                    "prix": this.state.prixDachat,
                    "imei": this.state.imei,
                    "prime": this.state.prime,
                    "type": this.state.value
                }),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(results => {
                    console.log(results)
                    return results.json()
                }).then(data => this.props.history.replace(`/add/FicheFinale`, { idClient: this.state.idclient, idappr: data.id, idrev: this.state.idrev }))
        }
    }

    handleChange = date => {
        this.setState({
            dateAchat: date
        });
    };
    handleChange1 = (e, { value }) => this.setState({ value })
    async componentWillMount() {
        await this.setState({ idclient: this.props.location.state.idClient, idrev: this.props.location.state.idrev })
        console.log(this.state.idclient)

    }
    setPrime(prix){
        this.setState({prixDachat:prix})
        let prime = 0
        if (prix > 200 && prix < 401) {
            prime = 30
        }
        else if (prix > 401 && prix < 851) {
            prime = 50
        }
        else if (prix > 851 && prix < 1251) {
            prime=80
        }
        else if (prix > 1251 && prix < 1701) {
            prime=120
        }
        else if (prix > 1701 && prix < 2201) {
            prime=160
        }
        else if (prix > 2201) {
            prime=200
        }
        this.setState({prime:prime})
    }
    render() {
        let types = ["GSM", "SMARTPHONE", "TABLETTE NUMERIQUE"]
        let appreils = ["ALCATEL", "DOOGEE", "HONOR", "IKU", "IPRO", "LENOVO", "NOKIA", "SAMSUNG", "TECNO MOBILE", "CONDOR", "EVERTEK", "IKU", "HUAWEI",
            "INFINIX", "LEAGOO", "LP", "OPPO", "SMARTEC", "VERSUS", "XIAOMI", "IPHONE", "MOTOROLA", "SAGEM", "LEAGOO", "SIEMENS", "ZTE"]
        return (
            <>
                <Header />
                {/* Page content */}
                <Container className="mt--7" fluid>
                    {/* Table */}
                    <Row>
                        <div className="col">
                            <Card className="shadow">
                                <CardHeader className="border-0">
                                    <h3 className="mb-0">Liste des appreils</h3>
                                </CardHeader>
                                <div style={{ marginTop: '5%' }}>
                                    <Form>
                                        <div style={{ width: 800, marginLeft: 50, marginTop: 20 }}>
                                            <h3 style={{ color: "#000000" }}>Date d'achat</h3>
                                            <ReactDatetime
                                                style={{ width: 800 }}
                                                defaultValue={this.state.dateAchat}
                                                inputProps={{
                                                    placeholder: "Date Picker Here"
                                                }}
                                                timeFormat={false}
                                                onChange={(e) => this.setState({ dateAchat: moment(e).format('YYYY-MM-DD') })}
                                            />
                                        </div>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.imei}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16, marginTop: 20 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder='Numéro IMEI'
                                                        type='numuric'
                                                        onChange={(text) => this.setState({ imei: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Dropdown style={{ marginLeft: 50, marginTop: 20 }} isOpen={this.state.drop1} toggle={() => this.setState({ drop1: !this.state.drop1 })}>
                                            <DropdownToggle caret>
                                                {this.state.value}
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                {
                                                    types.map(t =>
                                                        <DropdownItem key={t} onClick={() => this.setState({ value: t })}>{t}</DropdownItem>
                                                    )
                                                }
                                            </DropdownMenu>
                                        </Dropdown><br />
                                        <Dropdown style={{ marginLeft: 50, marginTop: 20 }} isOpen={this.state.drop2} toggle={() => this.setState({ drop2: !this.state.drop2 })}>
                                            <DropdownToggle caret>
                                                {this.state.marque}
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                {
                                                    appreils.map(t =>
                                                        <DropdownItem key={t} onClick={() => this.setState({ marque: t })}>{t}</DropdownItem>
                                                    )
                                                }
                                            </DropdownMenu>
                                        </Dropdown>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.modéle}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16, marginTop: 20 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder='Modéle'
                                                        type='text'
                                                        onChange={(text) => this.setState({ modéle: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.prixDachat}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16, marginTop: 20 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder='Prix d achat'
                                                        type='numuric'
                                                        onChange={(text) => this.setPrime(text.target.value)}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.prime}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16, marginTop: 20 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder='Prime'
                                                        type='numuric'
                                                        disabled
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    </Form>
                                    <Button
                                        color=" "
                                        type="button"
                                        style={{ width: "20%", marginLeft: "64%", marginTop: 20, marginBottom: 20, backgroundColor: "#fe0c00" }}
                                        onClick={() => this.ajout()}
                                    >
                                        <span style={{ color: "#fff" }}>Valider</span>
                                    </Button>
                                </div>
                            </Card>
                        </div>
                    </Row>
                </Container>
            </>
        );
    }
}

const style = {
    margin: 15,

};

export default Appreil;
