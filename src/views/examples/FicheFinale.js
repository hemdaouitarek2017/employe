import React, { Component } from 'react';
import tools from "../../services/apis.js"
import style from './style.css'
import { Checkbox } from 'semantic-ui-react'

class FicheFinale extends Component {
    constructor(props) {
        super(props);
        this.state = {
            puissan: '',
            marque: '',
            date: new Date(),
            modele: '',
            prixAchat: '',
            couleur: '',
            cylindrée: '',
            gsm: false,
            smart: false,
            tab: false,
            idclient: null,
            idappr: null,
            vehicule: null,
            client: null,
            assurance: null,
            show: false,
            idrev:null
        }
    }
    handleChange = date => {
        this.setState({
            date: date
        });
    };
    handleChange1 = date => {
        if (this.state.sousGarantie == 'Non')
            this.setState({
                sousGarantie: 'Oui'
            });
        else {
            this.setState({
                sousGarantie: 'Non'
            });
        }
    };
    async componentWillMount() {
        await this.setState({ idclient: this.props.location.state.idClient,idappr:this.props.location.state.idappr,idrev:1 })

        this.addAssur()
    }
    
    async addAssur() {
        await fetch('http://41.226.248.55:8080/apis/Garantie/'+this.state.idclient+"/"+this.state.idappr+"/"+this.state.idrev+"/1", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(results => {
                console.log(results)
                return results.json()
            }).then(data => {
                if (data.appreil.type == "SMARTPHONE") {
                    this.setState({ smart: true })
                }
                else if (data.appreil.type == "GSM") {
                    this.setState({ gsm: true })
                }
                else {
                    this.setState({ tab: true })
                }
                this.setState({ assurance: data, show: true })
            })
    }
    render() {
        if (this.state.show == true) {
            console.log(this.state.assurance)
            let idlength = this.state.assurance.id.toString().length
            let id = []
            for (let i = 0; i < 8 - idlength; i++) {
                id.push(<text className="infoDotted">0</text>)
            }
            let h = 0;
            let idstr = this.state.assurance.id.toString()
            for (let k = id.length; k < 8; k++) {
                id.push(<text className="infoDotted">{idstr[h]}</text>)
                h++
            }
            let cin = this.state.assurance.rentClientAppr.identite
            let cins = []
            for (let i = 1; i < 7; i++) {
                cins.push(<text className="infoStyles">{cin[i]}</text>)
            }
            let dateAchat = new Date(this.state.assurance.appreil.dateAchat)
            let achat = ((dateAchat.getMonth() > 8) ? (dateAchat.getMonth() + 1) : ('0' + (dateAchat.getMonth() + 1))) + '-' + ((dateAchat.getDate() > 9) ? dateAchat.getDate() : ('0' + dateAchat.getDate())) + "-" + dateAchat.getFullYear()
            let dateAch = []
            for (let i = 1; i < 9; i++) {
                dateAch.push(<text className="infoStyles">{achat[i]}</text>)
            }
            let imei = this.state.assurance.appreil.imei
            let imeils = []
            for (let i = 1; i < imei.length - 1; i++) {
                imeils.push(<text className="infoStyles">{imei[i]}</text>)
            }
            let faitle = new Date(this.state.assurance.faitle)
            let faitlee = '06-03-2020'
            let faitles = []
            for (let i = 1; i < faitlee.length; i++) {
                faitles.push(<text className="infoStyles">{faitlee[i]}</text>)
            }

            return (
                <div className="cont">
                    <text className="gar1">ATTESTATION DE GARANTIE</text><br /><br />
                    <text className="gar1" style={{ marginLeft: "20%" }}>Dommage Accidentelle</text><br /><br />

                    <div className="flexts1">
                        <text className="infoDotted">N</text>
                        {id}
                    </div>
                    <text className="titres">Renseignements concernement l'Adhérent(e)</text>
                    <br /><br />
                    <text className="labels" >Nom: </text>
                    <text className="labels1">{this.state.assurance.rentClientAppr.nom}</text>
                    <text className="labels" style={{ marginLeft: "5%" }}>Prenom:</text>
                    <text className="labels1">{this.state.assurance.rentClientAppr.prenom}</text>
                    <br /><br />
                    <text className="labels" style={{ marginBottom: "1%" }}>Numéro de cin:</text>
                    <text className="infoStyles" style={{ marginLeft: "1%" }}>{cin[0]}</text>
                    {
                        cins
                    }
                    <text className="infoStyles1">{cin[7]}</text><br />
                    <br />
                    <text className="labels">N° Téléphone: {this.state.assurance.rentClientAppr.modéle}</text>
                    <br /><br />
                    <text className="titres">Renseignements concernement l'appreil à garantir</text>
                    <br /><br />
                    <Checkbox checked={this.state.gsm} readOnly label={<label>GSM</label>} />
                    <Checkbox checked={this.state.smart} readOnly label={<label>SMARTPHONE</label>} />
                    <Checkbox checked={this.state.tab} readOnly label={<label>TABLETTE NUMERIQUE</label>} />

                    <br /><br />
                    <text className="labels">Date d'achat:</text>
                    <text className="infoStyles" style={{ marginLeft: "2%" }}> {achat[0]} </text>
                    {dateAch}
                    <text className="infoStyles1">{achat[9]}</text>
                    <br /><br />
                    <text className="labels">Marque:</text>
                    <text className="labels1" style={{ marginLeft: "5%" }}>{this.state.assurance.appreil.marque}</text>
                    <br /><br />
                    <text className="labels">Modéle:</text>
                    <text className="labels1" style={{ marginLeft: "5%" }}>{this.state.assurance.appreil.modéle}</text>
                    <br /><br />
                    <text className="labels">Prix d'Achat:</text>
                    <text className="labels1" style={{ marginLeft: "2%" }}>{this.state.assurance.appreil.prix}</text>
                    <text className="labels" style={{ marginLeft: "5%" }}>Prime :</text>
                    <text className="labels1" style={{ marginLeft: "2%" }}>{this.state.assurance.appreil.prime}</text>
                    <br /><br />
                    <text className="labels">N° IMEI:</text>
                    <text className="infoStyles" style={{ marginLeft: "5%" }}> {imei[0]} </text>
                    {imeils}
                    <text className="infoStyles1">{imei[imei.length - 1]}</text>
                    <br /><br />

                    <div id="grad">
                        <text id="desc">
                            Je reconnais avoir pris possession et connaissance de l'exemplaire de la Notice d'information du contrat « Garantie Casse Accidentelle des Produits Nomades » ci-jointe et en accepter les termes et conditions.
    Je certifie que les informations déclarées sur le présente Attestation de Garantie sont sincères et véritables et suis averti que toute réticence ou fausse déclaration intentionnelle de ma part, pourrait entraîner la déchéance de la garantie.
    Je suis informé qu'en cas de dommage, et sous peine de déchéance de mes droits , je suis tenu de déclarer le dommage dans les 5 jours ouvrés à compter de sa de survenance, et ce à la société AFRIQUE ASSISTANCE, sise à l’Immeuble TAMAYOUZ, 4ème étage Centre Urbain Nord, 1082 Tunis, en téléphonant au

                        </text>
                    </div>
                    <div style={{ marginTop: "4%" }}>
                        <text className="lab">Fait à : Tunis</text>
                        <text className="labels1" style={{ marginLeft: "3%" }}>{this.state.assurance.a}</text>
                        <text className="infoStyles" style={{ marginLeft: "4.7%" }}>{faitlee[0]}</text>
                        {faitles}
                        <text className="infoStyles1">{faitlee[9]}</text>
                        <text className="lab" style={{ marginLeft: "4%" }}>Signature :</text>
                        <text className="lab" style={{ marginLeft: "8%" }}>En cas de PANNE, veuillez contacter :</text><br />
                        <div className="flexts">
                            <div style={{ marginLeft: "33.5%" }} id="sig">
                            </div>
                            <div style={{ marginLeft: "6%" }} id="sig1">
                                <text style={{ marginLeft: "8%" }} id="call">AFRIQUE ASSISTANCE</text><br />
                                <text style={{ marginLeft: "23%" }} id="call">71 104 545</text>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        else {
            return (
                <div></div>
            )
        }

    }
}


export default FicheFinale;
