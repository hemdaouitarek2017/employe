import React from "react";
// react component that copies the given text inside your clipboard
import { CopyToClipboard } from "react-copy-to-clipboard";
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  Container,
  Col,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row, Button
} from "reactstrap";
// core components
import Header from "components/Headers/Header.js";
import ReactDatetime from "react-datetime";
import moment from "moment";
import tools from "../../services/apis.js"
class Icons extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      appreil: null,
      imei: null,
      modéle: null,
      marque: null,
      type: null,
      Prix: null,
      Prime: null,
      dateachat: null
    }
  }
  async componentWillMount() {
    let appreil = this.props.location.state.appreil
    await this.setState({
      appreil: appreil, imei: appreil.imei, modéle: appreil.modéle, marque: appreil.marque,
      type: appreil.type, Prix: appreil.prix, Prime: appreil.prime, dateachat: appreil.dateAchat
    })
  }

  async update() {
    console.log(this.state.Prix)
    await fetch(tools.setAppreil(this.state.appreil.id), {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "type": this.state.type,
        "dateAchat": this.state.dateachat,
        "marque": this.state.marque,
        "modéle": this.state.modéle,
        "imei": this.state.imei,
        "prix": this.state.Prix,
        "prime": this.state.Prime,
      }),
    })
      .then(results => {
        return results
      }).then(data => {
        console.log(data)
        this.props.history.replace("tables")
      })
  }
  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className=" mt--7" fluid>
          {/* Table */}
          <Row>
            <div className=" col">
              <Card className=" shadow">
                <CardHeader className=" bg-transparent">
                  <h3 className=" mb-0">{this.state.appreil.marque + " " + this.state.appreil.modéle}</h3>
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row>
                      <Col md="6">
                        <FormGroup >
                          <Input
                            value={this.state.marque}
                            style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                            id="exampleFormControlInput1"
                            placeholder="Marque"
                            type="text"
                            onChange={(text) => this.setState({ marque: text.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="6">
                        <FormGroup>
                          <Input
                            value={this.state.modéle}
                            style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                            id="exampleFormControlInput1"
                            placeholder="Modéle"
                            onChange={(text) => this.setState({ modéle: text.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="6">
                        <FormGroup>
                          <Input
                            value={this.state.type}
                            style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                            id="exampleFormControlInput1"
                            placeholder="Type"
                            onChange={(text) => this.setState({ type: text.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="6">
                        <FormGroup>
                          <Input
                            value={this.state.imei}
                            style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                            id="exampleFormControlInput1"
                            placeholder="Imei"
                            onChange={(text) => this.setState({ imei: text.target.value })}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <FormGroup>
                      <InputGroup style={{ width: 800, marginLeft: 50 }} className="input-group-alternative">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="ni ni-calendar-grid-58" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <ReactDatetime
                          value={this.state.dateachat}
                          inputProps={{
                            placeholder: "Date Picker Here"
                          }}
                          timeFormat={false}
                          onChange={(e) => this.setState({ dateachat: moment(e).format("YYYY-DD-MM") })}
                        />
                      </InputGroup>
                    </FormGroup>
                    <Row>
                      <Col md="6">
                        <FormGroup>
                          <Input
                            value={this.state.Prix}
                            style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                            id="exampleFormControlInput1"
                            placeholder="Prix"
                            onChange={(text) => this.setState({ Prix: text.target.value })}
                            type="number"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="6">
                        <FormGroup>
                          <Input
                            value={this.state.Prime}
                            style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                            id="exampleFormControlInput1"
                            placeholder="Prime"
                            onChange={(text) => this.setState({ Prime: text.target.value })}
                            type="number"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                  <div style={{ marginLeft: 640 }}>
                    <Button onClick={() => this.props.history.replace("tables")} color="danger" type="button"> Annuler</Button>
                    <Button onClick={() => this.update()} color="success" type="button">Modifier</Button>
                  </div>
                </CardBody>
              </Card>
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

export default Icons;
