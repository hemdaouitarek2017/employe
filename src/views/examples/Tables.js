/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  Pagination,
  PaginationItem,
  PaginationLink,
  Button,
  Table,
  Container,
  Row,
  Modal,
  FormGroup,
  Input,
  Dropdown, DropdownToggle, DropdownMenu, DropdownItem
} from "reactstrap";
// core components
import Header from "components/Headers/Header.js";
import tools from "../../services/apis.js"
import edit from "../../assets/img/edit.png"
import del from "../../assets/img/delete.png"
import ReactDatetime from "react-datetime";
import moment from "moment";
import ReactExport from "react-data-export";
import expo from "../../assets/img/export.png"
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
class Tables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 0,
      ListAppreil: [],
      ListRevendeur: [],
      ListDist: [],
      exampleModal: false,
      drop: false,
      label: "Choisie Un Revendeur",
      revSelct: null,
      drop1: false,
      label1: "Choisie Un Distrubuteur",
      distSelect: null,
      marque: null,
      datedebut: moment(),
      datefin: moment()
    };
  }
  handleClick(e, index) {

    e.preventDefault();

    this.setState({
      currentPage: index
    });

  }
  componentWillMount() {
    this.getApreils()
    this.getDist()
    this.pageSize = 10;
  }
  async getDist() {
    await fetch(tools.getDistrubuteur(1), {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(results => {
        return results.json()
      }).then(data => {
        console.log(data)
        this.setState({ ListDist: data })
      })
  }
  async getRevendeurs() {
    await fetch(tools.getRevendeur(), {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(results => {
        return results.json()
      }).then(data => {
        console.log(data)
        this.setState({ ListRevendeur: data })
      })
  }
  async getApreils() {
    await fetch(tools.getAppreilEmp(1), {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(results => {
        return results.json()
      }).then(data => {
        console.log(data)
        this.pagesCount = Math.ceil(data.length / this.pageSize);
        this.setState({ ListAppreil: data })
      })
  }
  async deleteAppreil(id) {
    await fetch(tools.setAppreil(id), {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(results => {
        return results
      }).then(data => {
        alert("Appreil a éte supprime")
        console.log(data)
        this.setState({
          ListAppreil: this.state.ListAppreil.filter(item => item.id !== id),
        });
      })
  }

  showRes(item) {
    if (item.del == 0) {
      if (moment().format("DD-MM-YYYY") == moment(this.state.datedebut).format("DD-MM-YYYY")
        && moment().format("DD-MM-YYYY") == moment(this.state.datefin).format("DD-MM-YYYY")) {
        return (
          <tr>
            <th scope="row">
              <span className="mb-0 text-sm">{item.marque}</span>
            </th>
            <td>{item.modéle}</td>
            <td>{item.type}</td>
            <td>{item.imei}</td>
            <td>{item.dateAchat}</td>
            <td>{item.prix}</td>
            <td>{item.prime}</td>
            <td>{item.distrubuteur.nomCommercial}</td>
            <div style={{ paddingTop: 10 }}>
              <Button style={{ height: 40, width: 40, alignItems: "center", justifyContent: "center" }} onClick={() => this.deleteAppreil(item.id)} className="btn-icon btn-2" color="danger" type="button">
                <span className="btn-inner--icon">
                  <img style={{ marginLeft: -10 }} src={del} />
                </span>
              </Button>
              <Button style={{ height: 40, width: 40, alignItems: "center", justifyContent: "center" }} onClick={() => this.props.history.replace(`/admin/ModifierAppreil`, { appreil: item })} className="btn-icon btn-2" color="success" type="button">
                <span className="btn-inner--icon">
                  <img style={{ marginLeft: -10 }} src={edit} />
                </span>
              </Button>
            </div>
          </tr>
        )
      }
      else {
        console.log(item.dateAchat)

        var debut = moment(this.state.datedebut).format("YYYY-MM-DD");
        var fin = moment(this.state.datefin).format("YYYY-MM-DD");
        var now = moment(item.dateAchat).format("YYYY-MM-DD");
        console.log(fin)
        console.log(now)
        console.log(debut)
        if (now > debut) {
          console.log("passe")
        } else {
          console.log("fur")
        }

        if (debut <= now && fin >= now) {
          return (
            <tr>
              <th scope="row">
                <span className="mb-0 text-sm">{item.marque}</span>
              </th>
              <td>{item.modéle}</td>
              <td>{item.type}</td>
              <td>{item.imei}</td>
              <td>{item.dateAchat}</td>
              <td>{item.prix}</td>
              <td>{item.prime}</td>
              <td>{item.distrubuteur.nomCommercial}</td>
              <div style={{ paddingTop: 10 }}>
                <Button style={{ height: 40, width: 40, alignItems: "center", justifyContent: "center" }} onClick={() => this.deleteAppreil(item.id)} className="btn-icon btn-2" color="danger" type="button">
                  <span className="btn-inner--icon">
                    <img style={{ marginLeft: -10 }} src={del} />
                  </span>
                </Button>
                <Button style={{ height: 40, width: 40, alignItems: "center", justifyContent: "center" }} onClick={() => this.props.history.replace(`/admin/ModifierAppreil`, { appreil: item })} className="btn-icon btn-2" color="success" type="button">
                  <span className="btn-inner--icon">
                    <img style={{ marginLeft: -10 }} src={edit} />
                  </span>
                </Button>
              </div>
            </tr>
          )
        }
      }
    }
  }
  async filter() {

    await fetch(tools.filter(), {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "marque": this.state.marque,
        "idrev": 1,
        "iddis": this.state.distSelect,
      }),
    })
      .then(results => {
        return results.json()
      }).then(data => {
        console.log(data)
        this.pagesCount = Math.ceil(data.length / this.pageSize);
        this.setState({ ListAppreil: data, exampleModal: false })
      })
  }
  render() {
    const { currentPage } = this.state;
    let data = []
    this.state.ListAppreil.map(t => {
      let field = []
      field = [{ value: t.marque }, { value: t.modéle }, { value: t.type }, { value: t.imei }, { value: t.dateAchat }, { value: t.prix }, { value: t.prime }, { value: t.distrubuteur.nomCommercial }, { value: t.revendeur.nom + " " + t.revendeur.prenom }]
      data.push(field)
    }
    )
    console.log(data)
    const multiDataSet = [
      {
        columns: [
          { title: "Marque", width: { wch: 20 } },//pixels width 
          { title: "Modéle", width: { wch: 20 } },//char width 
          { title: "Type", width: { wch: 20 } },
          { title: "IMEI", width: { wch: 20 } },
          { title: "Date d'achat", width: { wch: 20 } },
          { title: "Prix", width: { wch: 20 } },
          { title: "Prime", width: { wch: 20 } },
          { title: "Distrubuteur", width: { wch: 20 } },
          { title: "Revendeur", width: { wch: 20 } },
        ],
        data: data
      }
    ];
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Table */}
          <Row>
            <div className="col">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <h3 className="mb-0">Liste des appreils</h3>
                </CardHeader>
                <div>
                  <ExcelFile filename="List des appreils" element={<Button style={{ marginLeft: 20, marginBottom: 20 }} className="btn-icon btn-2" color="success" type="button">
                    <span className="btn-inner--icon">
                      <img src={expo} />
                      <span className="btn-inner--text">Export fichier excel</span>
                    </span>
                  </Button>}>
                    <ExcelSheet dataSet={multiDataSet} name="Organization" />
                  </ExcelFile>

                </div>
                {/* Modal */}
                <Modal
                  className="modal-dialog-centered"
                  isOpen={this.state.exampleModal}
                  toggle={() => this.setState({ exampleModal: true })}
                >
                  <div style={{ paddingTop: 20 }}>
                    <FormGroup >
                      <Input
                        value={this.state.marque}
                        style={{ marginLeft: 50, width: "80%", color: "#000000", fontSize: 16 }}
                        id="exampleFormControlInput1"
                        placeholder="Marque"
                        type="text"
                        onChange={(text) => this.setState({ marque: text.target.value })}
                      />
                    </FormGroup>

                    <div>
                      <Dropdown style={{ marginLeft: 50, marginTop: 20 }} isOpen={this.state.drop1} toggle={() => this.setState({ drop1: !this.state.drop1 })}>
                        <DropdownToggle caret>
                          {this.state.label1}
                        </DropdownToggle>
                        <DropdownMenu>
                          {
                            this.state.ListDist.map(t =>
                              <DropdownItem key={t.id} onClick={() => this.setState({ distSelect: t.id, label1: t.nomCommercial })}>{t.nomCommercial}</DropdownItem>
                            )
                          }
                        </DropdownMenu>
                      </Dropdown>
                    </div>
                    <div style={{ width: "80%", marginLeft: 50, marginTop: 20 }}>
                      <h3 style={{ color: "red" }}>Du</h3>
                      <ReactDatetime
                        defaultValue={this.state.datedebut}
                        inputProps={{
                          placeholder: "Date Picker Here"
                        }}
                        timeFormat={false}
                        onChange={(e) => this.setState({ datedebut: moment(e).format('YYYY-MM-DD') })}
                      />
                    </div>
                    <div style={{ width: "80%", marginLeft: 50, marginTop: 20 }}>
                      <h3 style={{ color: "red" }}>Au</h3>
                      <ReactDatetime
                        defaultValue={this.state.datefin}
                        inputProps={{
                          placeholder: "Date Picker Here"
                        }}
                        timeFormat={false}
                        onChange={(e) => this.setState({ datefin: moment(e).format('YYYY-MM-DD') })}
                      />
                    </div>
                  </div>
                  <div className="modal-footer">
                    <Button
                      color="secondary"
                      data-dismiss="modal"
                      type="button"
                      onClick={() => this.setState({ exampleModal: false })}
                    >
                      Ferme
            </Button>
                    <Button onClick={() => this.filter()} color="primary" type="button">
                      Chercher
            </Button>
                  </div>
                </Modal>

                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Marque</th>
                      <th scope="col">Modéle</th>
                      <th scope="col">Type</th>
                      <th scope="col" >imei</th>
                      <th scope="col">DateAchat</th>
                      <th scope="col">Prix</th>
                      <th scope="col" >Prime</th>
                      <th scope="col" >Dist</th>
                      <th scope="col" >Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.state.ListAppreil.slice(
                        currentPage * this.pageSize,
                        (currentPage + 1) * this.pageSize
                      ).map((t) => this.showRes(t))
                    }
                  </tbody>
                </Table>
                <div className="pagination-wrapper">
                  <Pagination style={{ padding: 10 }} className="pagination justify-content-end mb-0"
                    listClassName="justify-content-end mb-0">
                    <PaginationItem disabled={currentPage <= 0}>
                      <PaginationLink
                        onClick={e => this.handleClick(e, currentPage - 1)}
                        previous
                        href="#"
                      />
                    </PaginationItem>
                    {[...Array(this.pagesCount)].map((page, i) =>
                      <PaginationItem active={i === currentPage} key={i}>
                        <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                          {i + 1}
                        </PaginationLink>
                      </PaginationItem>
                    )}
                    <PaginationItem disabled={currentPage >= this.pagesCount - 1}>
                      <PaginationLink
                        onClick={e => this.handleClick(e, currentPage + 1)}
                        next
                        href="#"
                      />
                    </PaginationItem>
                  </Pagination>
                </div>
              </Card>
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

export default Tables;
