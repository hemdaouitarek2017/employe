/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
    Badge,
    Card,
    CardHeader,
    Pagination,
    PaginationItem,
    PaginationLink,
    Button,
    Table,
    Container,
    Row,
} from "reactstrap";
// core components
import Header from "components/Headers/Header.js";
import tools from "../../services/apis.js"
import edit from "../../assets/img/edit.png"
import del from "../../assets/img/delete.png"
import ReactExport from "react-data-export";
import expo from "../../assets/img/export.png"
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
class Rentclient extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 0,
            ListRent: []
        };
    }
    handleClick(e, index) {

        e.preventDefault();

        this.setState({
            currentPage: index
        });

    }
    componentWillMount() {
        this.getApreils()
        this.pageSize = 10;
    }
    async getApreils() {
        await fetch(tools.getRentEmpl(1), {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(results => {
                return results.json()
            }).then(data => {
                console.log(data)
                this.pagesCount = Math.ceil(data.length / this.pageSize);
                this.setState({ ListRent: data })
            })
    }
    async deleteRentClient(id) {
        await fetch(tools.deleteRentClient(id), {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(results => {
                return results
            }).then(data => {
                alert("Appreil a éte supprime")
                console.log(data)
                this.setState({
                    ListRent: this.state.ListRent.filter(item => item.id !== id),
                });
            })
    }
    showRes(item) {
        return (
            <tr>
                <td>{item.nom}</td>
                <td>{item.prenom}</td>
                <td>{item.telephone}</td>
                
            </tr>
        )
    }
    render() {
        const { currentPage } = this.state;
        let data = []
        this.state.ListRent.map(t => {
            let field = []
            field = [{ value: t.nom }, { value: t.prenom }, { value: t.identite }, { value: t.sexe }, { value: t.ville }, { value: t.adresse }, { value: t.telephone }, { value: t.revendeur.nom + " " + t.revendeur.prenom }]
            data.push(field)
        }
        )
        console.log(data)
        const multiDataSet = [
            {
                columns: [
                    { title: "Nom", width: { wch: 20 } },//pixels width 
                    { title: "Prénom", width: { wch: 20 } },//char width 
                    { title: "Identité", width: { wch: 20 } },
                    { title: "Sexe", width: { wch: 20 } },
                    { title: "Ville", width: { wch: 20 } },
                    { title: "Adresse", width: { wch: 20 } },
                    { title: "Télephone", width: { wch: 20 } },
                    { title: "Revendeur", width: { wch: 20 } },
                ],
                data: data
            }
        ];
        return (
            <>
                <Header />
                {/* Page content */}
                <Container className="mt--7" fluid>
                    {/* Table */}
                    <Row>
                        <div className="col">
                            <Card className="shadow">
                                <CardHeader className="border-0">
                                    <h3 className="mb-0">Liste des garanties</h3>
                                </CardHeader>
                                <ExcelFile filename="List des clients" element={<Button style={{ marginLeft: 20, marginBottom: 20 }} className="btn-icon btn-2" color="success" type="button">
                                    <span className="btn-inner--icon">
                                        <img src={expo} />
                                        <span className="btn-inner--text">Export fichier excel</span>
                                    </span>
                                </Button>}>
                                    <ExcelSheet dataSet={multiDataSet} name="Organization" />
                                </ExcelFile>
                                <Table className="align-items-center table-flush" responsive>
                                    <thead className="thead-light">
                                        <tr>
                                        <th scope="col">Nom</th>
                                            <th scope="col">Prenom</th>
                                            <th scope="col">Télephone</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            (this.state.ListRent.length) > 0
                                                ?
                                                this.state.ListRent.slice(
                                                    currentPage * this.pageSize,
                                                    (currentPage + 1) * this.pageSize
                                                ).map((t) => this.showRes(t))
                                                :
                                                <div></div>
                                        }
                                    </tbody>
                                </Table>
                                {
                                    (this.state.ListRent.length) > 0
                                        ?
                                        <div className="pagination-wrapper">
                                            <Pagination style={{ padding: 10 }} className="pagination justify-content-end mb-0"
                                                listClassName="justify-content-end mb-0">
                                                <PaginationItem disabled={currentPage <= 0}>
                                                    <PaginationLink
                                                        onClick={e => this.handleClick(e, currentPage - 1)}
                                                        previous
                                                        href="#"
                                                    />
                                                </PaginationItem>
                                                {[...Array(this.pagesCount)].map((page, i) =>
                                                    <PaginationItem active={i === currentPage} key={i}>
                                                        <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                                                            {i + 1}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                )}
                                                <PaginationItem disabled={currentPage >= this.pagesCount - 1}>
                                                    <PaginationLink
                                                        onClick={e => this.handleClick(e, currentPage + 1)}
                                                        next
                                                        href="#"
                                                    />
                                                </PaginationItem>
                                            </Pagination>
                                        </div>
                                        :
                                        <div></div>
                                }
                            </Card>
                        </div>
                    </Row>
                </Container>
            </>
        );
    }
}

export default Rentclient;
